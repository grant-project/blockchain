package agent;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AgentManager {

    private List<Agent> agents = new ArrayList<>();
    private static final Block root = new Block(0, "ROOT_HASH", "ROOT", "ROOT");

    public Agent addAgent(String name, int port) {
        if(getAgent(name) == null) {
            Agent a = new Agent(name, "localhost", port, root, agents);
            a.startHost();
            agents.add(a);
            return a;
        } else return null;
    }

    public Agent getAgent(String name) {
        for (Agent a : agents) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }

    public List<Agent> getAllAgents() {
        return agents;
    }

    public void deleteAgent(String name) {
        final Agent a = getAgent(name);
        if (a != null) {
            a.stopHost();
            agents.remove(a);
        }
    }

    public List<Block> getAgentBlockchain(String name) {
        final Agent agent = getAgent(name);
        if (agent != null) {
            return agent.getBlockchain();
        }
        return null;
    }

    public void deleteAllAgents() {
        for (Agent a : agents) {
            a.stopHost();
        }
        agents.clear();
    }

    public Block createBlock(final String name, final String price) {
        final Agent agent = getAgent(name);
        if (agent != null) {
            String appConfigPath = getClass().getResource("/local.properties").getPath();
            Properties appProps = new Properties();

            try {
                appProps.load(new FileInputStream(appConfigPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (appProps.getProperty(Const.getName() + ".energy") != null) {
                Integer oldEnergy = Integer.valueOf(appProps.getProperty(Const.getName() + ".energy"));
                Integer diff = Integer.valueOf(price);
                appProps.setProperty(Const.getName() + ".energy", String.valueOf(oldEnergy - diff));
                OutputStream out = null;
                try {

                    Properties props = new Properties();

                    FileInputStream input = new FileInputStream(new File(appConfigPath));
                        props.load(new InputStreamReader(input, StandardCharsets.UTF_8));
                        //Change your values here
                        props.setProperty(Const.getName() + ".energy", String.valueOf(oldEnergy - diff));

                    OutputStreamWriter writer =
                                 new OutputStreamWriter(new FileOutputStream(appConfigPath), StandardCharsets.UTF_8);
                    // do stuff

                    //out = new FileOutputStream( new File(appConfigPath) );
                    props.store(writer, "This is an optional header comment string");

                    System.out.println(props.get("ServerPort"));

                }
                catch (Exception e ) {
                    e.printStackTrace();
                }
                finally{

                    if(out != null){

                        try {

                            out.close();
                        }
                        catch (IOException ex) {

                            System.out.println("IOException: Could not close myApp.properties output stream; " + ex.getMessage());
                            ex.printStackTrace();
                        }
                    }
                }
                //return (appProps.getProperty(Const.getName() + ".energy"));
            }
            return agent.createBlock(price);
        }
        return null;
    }
}
