package agent;

public class Const {
    public static String userName = "";
    public static String userPublicName = "";

    public static String getName() {
        return userName;
    }

    public void setName(String name) {
        this.userName = name;
    }

    public String getPublicName() {
        return userPublicName;
    }

    public void setPublicName(String name) {
        this.userPublicName = name;
    }

    public void logout() {
        this.userName = "";
        this.userPublicName = "";
    }
}
