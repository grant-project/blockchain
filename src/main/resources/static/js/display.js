"use strict";

const OUTPUT_TABLE_NAME = "output";

function displayMsg(text, color) {
    if (color === undefined) {
        color = "black";
    }
    document.getElementById("msg").innerHTML = text;
    document.getElementById("msg").style.color = color;
}

function displayAllAgents(json) {
    cleanTable(OUTPUT_TABLE_NAME);
    getVaultSize(document.getElementById('hello-user-name').innerHTML,document.getElementById('hello-user-name-hide').innerHTML );

    var agents;
    try {
        agents = JSON.parse(json);
    } catch (e) {
        displayMsg("Invalid response from server " + json, "red");
        return;
    }
    for (var i in agents) {
        displayAgent(agents[i]);
    }
}
function getAuthResult(result) {
    if (result == "ERROR") {
        document.getElementById("auth-login").value = "";
        document.getElementById("auth-pass").value = "";
        alert('Неверные данные для входа');

    } else {
        //alert('success');
        window.location.replace("/index.html");


    }
}

function displayUserName(userName) {
    document.getElementById("hello-user-name").innerHTML = userName;
}

function displayUserNameHide(userName) {
    document.getElementById("hello-user-name-hide").innerHTML = userName;
    getVaultSize(document.getElementById('hello-user-name').innerHTML,document.getElementById('hello-user-name-hide').innerHTML );
}

function displayVaultSize(size) {
    document.getElementById("vault-user-size").innerHTML = " " + size + " кВт" ;
    document.getElementById("sellRange").max = size ;
    document.getElementById("vault-user-size2").innerHTML = " " + size + " кВт" ;
    document.getElementById("vault-user-size3").innerHTML = " " + size + " кВт" ;
}

function displaySellVault(size) {
    document.getElementById("sell-energy").innerHTML = " " + size + " кВт" ;
}

function displayBuyVault(size) {
    document.getElementById("buy-energy").innerHTML = " " + size + " кВт" ;
}
function successLogout() {
    window.location.replace("/");
}
function displayAgent(jsonAgent) {
    if (typeof jsonAgent === "string") {
        var agent;
        try {
            agent = JSON.parse(jsonAgent);
        } catch (e) {
            displayMsg("Invalid response from server " + jsonAgent, "red");
            return;
        }
    } else {
        agent = jsonAgent;
    }

    var idx = 0;
    var idy = 1;
    var table = document.getElementById(OUTPUT_TABLE_NAME);
    var row = table.insertRow(table.length);
    var chain = agent.blockchain;
    var nameCell = row.insertCell(idx++);

    var timeCell = row.insertCell(idx++);
    var hashCell = row.insertCell(idx++);
    var blockchainCell = row.insertCell(idx++);

    for (var i in chain) {
        var row = table.insertRow(table.length);
        appendRow(agent, i);
        // nameCell.title = agent.name;
        // timeCell.innerHTML = timestampToDate(agent.blockchain[i].timestamp);
        // nameCell.innerHTML = agent.name;
        // hashCell.innerHTML = agent.blockchain[i].hash;
        idx++;
        //blockchainCell.appendChild(createBlockP(chain[i]));

    }
    blockchainCell.className = "blockchain";
    var p = document.createElement("P");
    p.appendChild(addCellButton("Mine", function () {
        mine(agent.name);
    }));
    p.appendChild(addCellButton("Delete", function () {
        deleteAgent(agent.name);
    }));
    row.insertCell(idx).appendChild(p);

    function addCellButton(name, onclick) {
        var button = document.createElement("BUTTON");
        button.className = "cellButton";
        button.appendChild(document.createTextNode(name));
        button.onclick = onclick;
        return button;
    }
}

function appendRow(agent, i) {
    var tbl = document.getElementById(OUTPUT_TABLE_NAME), // table reference
        row = tbl.insertRow(tbl.rows.length),      // append table row
        i;
    // insert table cells to the new row
    //for (i = 0; i < tbl.rows[0].cells.length; i++) {
        createCell(row.insertCell(0), agent.blockchain[i].creator);
    createCell(row.insertCell(1),  timestampToDate(agent.blockchain[i].timestamp));
    createCell(row.insertCell(2),  agent.blockchain[i].hash);
    createCell(row.insertCell(3),  agent.blockchain[i].previousHash);
    createCell(row.insertCell(4),  agent.blockchain[i].price);
    //}
}
function createCell(cell, text) {
    var div = document.createElement('div'), // create DIV element
        txt = document.createTextNode(text); // create text node
    div.appendChild(txt);                    // append text node to the DIV
    cell.appendChild(div);                   // append DIV to the table cell
}
function timestampToDate(timestamp) {
    var a = new Date(timestamp);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
}

function displayBlock(jsonBlock) {
    if (typeof jsonBlock === "string") {
        var block;
        try {
            block = JSON.parse(jsonBlock);
        } catch (e) {
            document.getElementById("msg").innerHTML = "Invalid response from server " + jsonBlock;
            return;
        }
    } else {
        block = jsonBlock;
    }
    displayMsg("New block mined:<br>" + getBlockString(block), "green");
}

function getBlockString(block) {
    return "index=" + block.index + " creator=" + block.creator + " timestamp="
        + block.timestamp + " hash=" + block.hash + " previous hash=" + block.previousHash + "<br>";
}

function createBlockP(block) {
    var p = document.createElement("P");
    p.title = "creator " + block.creator;
    p.innerHTML = "index=" + block.index + " creator=" + block.creator + " timestamp="
        + block.timestamp + " hash=" + block.hash + " previous hash=" + block.previousHash;
    console.log("create p.innerHTML" + p.innerHTML);
    return p;
}

function cleanTable(name) {
    var table = document.getElementById(name);
    table.innerHTML = "";
    var row = table.insertRow(0);
    var idx = 0;
    row.insertCell(idx++).innerHTML = "Пользователь";
    row.insertCell(idx++).innerHTML = "Дата сделки";
    row.insertCell(idx++).innerHTML = "Хэш";
    row.insertCell(idx++).innerHTML = "Прошлый хэш";
    row.insertCell(idx).innerHTML = "Количество кВт";
}