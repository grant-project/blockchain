package web;

import agent.Agent;
import agent.AgentManager;
import agent.Block;
import agent.Const;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Properties;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(path = "/agent")
@SessionAttributes(types = Const.class)
public class AgentController {

    private static AgentManager agentManager = new AgentManager();

    @RequestMapping(method = GET)
    public Agent getAgent(@RequestParam("name") String name) {
        return agentManager.getAgent(name);
    }

    @RequestMapping(method = DELETE)
    public void deleteAgent(@RequestParam("name") String name) {
        agentManager.deleteAgent(name);
    }

    @RequestMapping(method = POST, params = {"name", "port"})
    public Agent addAgent(@RequestParam("name") String name, @RequestParam("port") int port) {
        return agentManager.addAgent(name, port);
    }

    @RequestMapping(path = "all", method = GET)
    public List<Agent> getAllAgents() {
        return agentManager.getAllAgents();
    }

    @RequestMapping(path = "all", method = DELETE)
    public void deleteAllAgents() {
        agentManager.deleteAllAgents();
    }

    @RequestMapping(path = "getname", method = GET)
    public String getUserName() {
        return Const.userPublicName;
    }

    @RequestMapping(path = "getlogin", method = GET)
    public String getUserLogin() {
        return Const.userName;
    }

    @RequestMapping(path = "getenergy", method = GET)
    public String getUserEnergy(@RequestParam("username") String username, @RequestParam("fullname") String fullname) {
        String appConfigPath = getClass().getResource("/local.properties").getPath();
        Const.userName = fullname;
        Const.userPublicName = username;
        Properties appProps = new Properties();

        try {
            appProps.load(new FileInputStream(appConfigPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (appProps.getProperty(fullname + ".energy") != null) {
            return appProps.getProperty(fullname + ".energy");
        } else return "ERROR";
    }

    @RequestMapping(path = "setenergy", method = POST, params = {"energy"})
    public String setUserEnergy(@RequestParam("energy") String energy) {
        String appConfigPath = getClass().getResource("/local.properties").getPath();
        Properties appProps = new Properties();

        try {
            appProps.load(new FileInputStream(appConfigPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (appProps.getProperty(Const.getName() + ".energy") != null) {
            Integer oldEnergy = Integer.valueOf(appProps.getProperty(Const.getName() + ".energy"));
            Integer diff = Integer.valueOf(energy);
           appProps.setProperty(Const.getName() + ".energy", String.valueOf(oldEnergy - diff));
            return (appProps.getProperty(Const.getName() + ".energy"));
        } else return "ERROR";
    }

    @RequestMapping(path = "auth", method = GET)
    public String getAuth() {
        return Const.getName() == "" ? "ERROR" : Const.userPublicName;
    }

    @RequestMapping(path = "logout", method = DELETE)
    public String logout() {
        Const.userName = "";
        Const.userPublicName = "";
        return "OK";
    }

    @RequestMapping(path = "auth", method = POST, params = {"username", "passwd"})
    public String setAuth(@RequestParam("username") String username, @RequestParam("passwd") String passwd, Const consts) {
        String appConfigPath = getClass().getResource("/local.properties").getPath();
        Properties appProps = new Properties();

        try {
            appProps.load(new FileInputStream(appConfigPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (appProps.getProperty(username + ".pass") == null) {
            return "ERROR";
        }
        if (appProps.getProperty(username + ".pass").equals(passwd)) {
            String fullname = "";
            try {
                fullname = new String(appProps.getProperty(username + ".name").getBytes("ISO-8859-1"), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            consts.setPublicName(fullname);
            consts.setName(username);
            return Const.userPublicName;
        } else {
            return "ERROR";
        }

    }

    @RequestMapping(method = POST, path = "mine")
    public Block createBlock(@RequestParam(value = "agent") final String name, @RequestParam(value = "price") final String price) {
        return agentManager.createBlock(name, price);
    }
}